import pandas as pd
from elasticsearch import Elasticsearch

code = 'LB30'
#code = 'LB31'
#code = 'PS11'
#code = 'PS12'
#code = 'PS13'


df = pd.read_table('../'+code+'_u_ex171102.log', delimiter=' ')

df['date'] = df['date'] + 'T' + df['time']

es = Elasticsearch([{'host': 'http://elastic:changeme@127.0.0.1', 'port': 9200}])

for i in range(len(df)):
    es.index(index='postnl-test-'+code.lower(), doc_type='logs', id=i, body=df.iloc[i].to_json())
    if i%50==0:
    	print(i)
