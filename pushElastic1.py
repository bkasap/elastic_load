import pandas as pd
from elasticsearch import Elasticsearch
import numpy as np

#code, starttime = 'LB30', '10:58:14'
#code, starttime = 'LB31', '10:36:01'
#code, starttime = 'PS11', '11:38:13'
#code, starttime = 'PS12', '11:45:40'
#code, starttime = 'PS13', '10:55:12'
code, starttime = 'psdk7', '00:00:00'

#LB columns
#name = ['date', 'time', 's-ip', 'cs-method', 'cs-uri-stem', 'cs-uri-query', 's-port', 'cs-username', 'c-ip', 'cs(User-Agent)', 'cs(Referer)', 'sc-status', 'sc-substatus', 'sc-win32-status', 'time-taken', 'OriginalIP']

#PS columns
name = ['date', 'time', 's-ip', 'cs-method', 'cs-uri-stem', 'cs-uri-query', 's-port', 'cs-username', 'c-ip', 'cs(User-Agent)', 'cs(Referer)', 'sc-status', 'sc-substatus', 'sc-win32-status', 'time-taken']

#df = pd.read_table('../new/'+code+'_u_ex171102.log', delimiter=' ', comment='#', names=name, encoding='latin_1')

df = pd.read_table('../new/u_ex171107.log', delimiter=' ', comment='#', names=name, encoding='latin_1')

df['date'] = df['date'] + 'T' + df['time']

es = Elasticsearch([{'host': '127.0.0.1', 'port': 9200}], http_auth = ('elastic','changeme'))

#for i in range(np.where(df['time']==starttime)[0][0], len(df)):
for i in range(len(df)):
	es.index(index='postnl-test-'+code.lower(), doc_type='logs', id=i, body=df.iloc[i].to_json())
	if i%50==0:
		print(i, '\ncompleted')


#print("success")

#print(df)
